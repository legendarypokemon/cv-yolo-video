import cv2
import numpy as np
from numpy.core.fromnumeric import argmax
import funcs_video
import pandas as pd

labels_map = pd.read_csv('models/yolov3/class_labels.txt', header=None)[0].values

net = cv2.dnn.readNetFromDarknet('models/yolov3/yolov3.cfg', 'models/yolov3/yolov3.weights')
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)

cap = cv2.VideoCapture('videos/LeMans.mp4')

if cap.isOpened() == False:
    print('Error opening video stream or file')

while(cap.isOpened()):
    
    ret, frame = cap.read()
    
    (h, w) = frame.shape[:2]
    if ret == True:
        blob = cv2.dnn.blobFromImage(image=frame, scalefactor=1/255, size=(416, 416), swapRB=True, crop=False)
        net.setInput(blob)
        detections = net.forward()
        
        boxes = detections[:, :4].copy()

        probs = detections[:, 5:].max(axis=1)

        labels_idx = detections[:, 5:].argmax(axis=1)
        labels = np.array([labels_map[i] for i in labels_idx])
        
        del labels_idx
        rows = np.where(probs > 0.5)

        detections = detections[rows]
        boxes = boxes[rows]
        probs = probs[rows]
        labels = labels[rows]

        detections = detections[np.argsort(-probs)]
        boxes = boxes[np.argsort(-probs)]
        labels = labels[np.argsort(-probs)]
        probs = probs[np.argsort(-probs)]

        boxes = boxes * np.array([w, h, w, h])

        corners = np.zeros_like(boxes)
        corners[:, 0] = boxes[:, 0] - boxes[:, 2] / 2
        corners[:, 1] = boxes[:, 1] - boxes[:, 3] / 2
        corners[:, 2] = corners[:, 0] + boxes[:, 2]
        corners[:, 3] = corners[:, 1] + boxes[:, 3]
        corners = corners.astype(int)

        if detections.shape[0] > 0:
            detections_filtered = detections[:1, :].copy()
            boxes_filtered = boxes[:1, :].copy()
            labels_filtered = labels[:1].copy()
            probs_filtered = probs[:1].copy()
            corners_filtered = corners[:1, :].copy()

            for i in range(detections.shape[0]):
                
                include = True
                for j in range(detections_filtered.shape[0]):                    
                    if (probs[i] <= probs_filtered[j]) & (labels[i] == labels_filtered[j]) & (funcs_video.iou(corners[i, :], corners_filtered[j, :]) > 0.5):
                        include = False
                        
                if include:
                    
                    detections_filtered = np.concatenate((detections_filtered, detections[i:i+1, :]), axis=0)
                    labels_filtered = np.concatenate((labels_filtered, labels[i:i+1]), axis=0)
                    probs_filtered = np.concatenate((probs_filtered, probs[i:i+1]), axis=0)
                    boxes_filtered = np.concatenate((boxes_filtered, boxes[i:i+1, :]), axis=0)
                    corners_filtered = np.concatenate((corners_filtered, corners[i:i+1, :]), axis=0)

            for i in range(detections_filtered.shape[0]):

                rect = cv2.rectangle(frame, (corners_filtered[i,0], corners_filtered[i,1]), (corners_filtered[i,2], corners_filtered[i,3]), (0, 255, 0), 1)
                text = cv2.putText(frame, (labels_filtered[i] + ': ' + str(np.round(probs_filtered[i], 2))), (corners_filtered[i, 0], corners_filtered[i, 1]), cv2.FONT_HERSHEY_PLAIN, 1, color=(255, 0, 0))

        cv2.imshow('Frame', frame)

        if cv2.waitKey(5) & 0xFF == ord('q'):
            break

    else:
        break

cap.release()

cv2.destroyAllWindows()