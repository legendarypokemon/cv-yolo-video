import numpy as np

def iou(box1, box2):
        
    x_left = max(box1[0], box2[0])
    x_right = min(box1[2], box2[2])
    y_top = max(box1[1], box2[1])
    y_bottom = min(box1[3], box2[3])
    
    if (x_left >= x_right) | (y_bottom <= y_top):
        return 0.0
    
    intersection = (x_right - x_left) * (y_bottom - y_top)
    union = (box1[2] - box1[0]) * (box1[3] - box1[1]) + (box2[2] - box2[0]) * (box2[3] - box2[1])

    IoU = intersection / (union - intersection)

    return IoU