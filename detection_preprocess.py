import numpy as np

def rect_transform(rect):

    if isinstance(rect, np.ndarray) == False:
        raise TypeError('rect parameter must have type np.ndarray')
    
    if rect.shape != (4,):
        raise ValueError('rect parameter has shape ' + str(rect.shape) + ' instead of (4,)')
    
    if all(isinstance(i, (int, np.integer)) for i in rect) == False and all(isinstance(i, (float, np.floating)) for i in rect) == False:
        raise TypeError('rect elemants must have type int or float')

    if all(i >= 0 for i in rect) == False:
        raise TypeError('rect elemants must be positive')
    
    diag = np.zeros_like(rect)
    diag[0] = rect[0] - rect[2] / 2
    diag[1] = rect[1] - rect[3] / 2
    diag[2] = diag[0] + rect[2]
    diag[3] = diag[1] + rect[3]
    diag = diag.astype(int)

    return diag

def iou(box1, box2):
        
    x_left = max(box1[0], box2[0])
    x_right = min(box1[2], box2[2])
    y_top = max(box1[1], box2[1])
    y_bottom = min(box1[3], box2[3])
    
    if (x_left >= x_right) | (y_bottom <= y_top):
        return 0.0
    
    intersection = (x_right - x_left) * (y_bottom - y_top)
    union = (box1[2] - box1[0]) * (box1[3] - box1[1]) + (box2[2] - box2[0]) * (box2[3] - box2[1])

    IoU = intersection / (union - intersection)

    return IoU